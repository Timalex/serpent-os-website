---
title: "Reliability you can Count on"
date: 2021-06-21T09:54:13+10:00
draft: false
Callout: "Our mission is to keep you running without the hassle"
---

The Serpent OS team understands that there's more to life than making an OS. Our mission is to keep you running without
the hassle so you can get done what you want to do.

The ways Serpent OS makes a rolling release safer is by:

 - Read only rootfs for improved security
   - The layout still feels like a normal Linux distribution
 - Atomic updates + rollbacks with a seriously powerful package manager
   - Boot into the previous kernel and system root to be back to where you were before the update
 - Versioned repos so you can shift back to an old version till **you're** ready to move on
   - Perfect for testing - recreating a system to reproduce a specific bug and having user testing before becoming stable
   - Return to a prior version that you never even installed
   - Switch back and forth between feature branches
 - Integration of testing packages
   - Checks that dependencies are correct and basic functionality to avoid unnecessary errors
